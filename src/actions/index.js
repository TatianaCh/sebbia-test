import * as ActionTypes from '../constants';
import config from '../config';

function loadCategories() {
  return (dispatch) => {
    fetch(`${config.apiPrefix}/news/categories`)
      .then(response => response.json())
      .then(data => {
        dispatch({
            type: ActionTypes.NEWS_CATEGORIES_LOADED,
            payload: data,
        });
       })
      .catch(err => {
        throw err;
      });
  };
}

function loadNews(categoryId, page = 0) {
  return (dispatch) => {
    fetch(`${config.apiPrefix}/news/categories/${categoryId}/news?page=${page}`)
      .then(response => response.json())
      .then(data => {
        dispatch({
            type: ActionTypes.NEWS_LOADED,
            payload: data,
        });
       })
      .catch(err => {
        throw err;
      });
  };
}

function setArticle(article) {
  return {
      type: ActionTypes.SET_ARTICLE,
      payload: article,
  };
}

function loadArticle(articleId) {
  return (dispatch) => {
    fetch(`${config.apiPrefix}/news/details?id=${articleId}`)
      .then(response => response.json())
      .then(data => {
        dispatch({
            type: ActionTypes.ARTICLE_LOADED,
            payload: data,
        });
       })
      .catch(err => {
        throw err;
      });
  };
}

export default {
  loadCategories,
  loadNews,
  loadArticle,
  setArticle
};

