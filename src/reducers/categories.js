import * as ActionTypes from '../constants';

const initialState = {};

export default function categories(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.NEWS_CATEGORIES_LOADED:
      {
        return { ...state, ...action.payload };
      }
    default:
      return state;
  }
};
