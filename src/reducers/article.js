import * as ActionTypes from '../constants';

const initialState = {};

export default function article(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.SET_ARTICLE:
      {
        return { ...state, news: {...action.payload} };
      }
    case ActionTypes.ARTICLE_LOADED:
      {
        return { ...state, ...action.payload };
      }
    default:
      return state;
  }
};
