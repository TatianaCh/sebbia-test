import * as ActionTypes from '../constants';

const initialState = {};

export default function news(state = initialState, action) {
  switch (action.type) {
    case ActionTypes.NEWS_LOADED:
      {
        return { ...state, ...action.payload };
      }
    default:
      return state;
  }
};
