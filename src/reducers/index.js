import { combineReducers } from 'redux';
import categories from './categories';
import news from './news';
import article from './article';


export default combineReducers({
  categories,
  news,
  article,
})