import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import icons from '../images/svg-icons';
import viewType from '../constants/viewType';
import actions from '../actions';
import CategoriesView from '../components/CategoriesView';
import NewsView from '../components/NewsView';
import ArticleView from '../components/ArticleView';
import './MainPage.scss';

class MainPage extends Component {
  static propTypes = {
    categories: PropTypes.object.isRequired,
    news: PropTypes.object.isRequired,
    article: PropTypes.object.isRequired,
    loadCategories: PropTypes.func.isRequired,
    loadNews: PropTypes.func.isRequired,
    loadArticle: PropTypes.func.isRequired,
    setArticle: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.getNewsHandler = this.getNewsHandler.bind(this);
    this.state = {
      view: viewType.categories,
      selectedCategory: null,
    };
  }

  componentDidMount() {
    this.props.loadCategories();
  }

  getNewsHandler(categoryId, page = 0) {
    if (this.state.view !== viewType.news) {
      this.setState({
        view: viewType.news,
        selectedCategory: categoryId,
      });
    }
  }

  onArticleClick = article => () => {
    this.props.setArticle(article);
    this.setState({
      view: viewType.article,
    });
    this.props.loadArticle(article.id);
  }

  onChangeView = view => () => {
    if (Object.values(viewType).includes(view)) {
      this.setState({
        view,
      });
    }
  }

  render() {
    const { view, selectedCategory } = this.state;
    const { categories, news, article, loadNews } = this.props;
    let pageContent = false;
    let breadCrumbs = [{
      name: 'Категории',
    }];
    switch (view) {
      case viewType.news: {
        breadCrumbs = [
          {
            name: 'Категории',
            onClick: this.onChangeView(viewType.categories),
          },
          {
            name: 'Новости',
          },
        ];
        pageContent = (
          <NewsView
            data={news}
            loadNews={loadNews}
            selectedCategory={selectedCategory}
            onItemClick={this.onArticleClick}
            backHandler={this.onChangeView(viewType.categories)}
          />
        );
        break;
      }
      case viewType.article: {
        breadCrumbs = [
          {
            name: 'Категории',
            onClick: this.onChangeView(viewType.categories),
          },
          {
            name: 'Новости',
            onClick: this.onChangeView(viewType.news),
          },
          {
            name: 'Статья',
          },
        ];
        pageContent = (<ArticleView data={article} />);
        break;
      }
      default: {
        pageContent = (
          <CategoriesView
            data={categories}
            onItemClick={this.getNewsHandler}
          />
        );
      }
    };

    const breadCrumbsList = breadCrumbs.map(item => (
      <li key={item.name} onClick={item.onClick}>{item.name}</li>
    ));

    return (
      <div className="root">
        <div className="svgContainer" dangerouslySetInnerHTML={{ __html: icons }} />
        <div className="content">
          <ul className="breadcrumbs">{breadCrumbsList}</ul>
          {pageContent}
        </div>
      </div>
    );
  }
}


const mapStateToProps = state => ({
  ...state,
});

const mapActions = {
  ...actions,
};

const MainPageHOC = connect(mapStateToProps, mapActions)(MainPage);

export default MainPageHOC;
