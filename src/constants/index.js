/* eslint-disable import/prefer-default-export */


export const NEWS_CATEGORIES_LOADED = 'NEWS_CATEGORIES_LOADED';
export const NEWS_LOADED = 'NEWS_LOADED';
export const ARTICLE_LOADED = 'ARTICLE_LOADED';
export const SET_ARTICLE = 'SET_ARTICLE';
