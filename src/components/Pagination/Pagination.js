import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import './Pagination.scss';


class Pagination extends Component {
  static propTypes = {
    page: PropTypes.number.isRequired,
    nextEnabled: PropTypes.bool.isRequired,
    next: PropTypes.func.isRequired,
    prev: PropTypes.func.isRequired,
  };

  static get defaultProps() {
    return {
      nextEnabled: true,
    };
  }

  render() {
    const { page, nextEnabled, next, prev } = this.props;
    const prevArrowClass = page > 0 ? 'arrow prev' : 'arrow prev disabled';
    const nextArrowClass = nextEnabled ? 'arrow next' : 'arrow next disabled';
    return (
      <div className="pagination">
        <div className={prevArrowClass} onClick={prev}>
          <svg>
            <use xlinkHref="#icon-right-chevron" />
          </svg>
        </div>
        <div className="page">Page {page + 1}</div>
        <div className={nextArrowClass} onClick={next}>
          <svg>
            <use xlinkHref="#icon-right-chevron" />
          </svg>
        </div>
      </div>
    );
  }
}

export default Pagination;
