import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ProgressLoader from '../ProgressLoader';
import './CategoriesView.scss';


class CategoriesView extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    onItemClick: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isProgressVisible: !('code' in props.data),
    };
  }

  componentWillReceiveProps(nextProps) {
    if ('code' in nextProps.data) {
      this.setState({
        isProgressVisible: false,
      });
    }
  }

  onCategoryClick = categoryId => () => {
    this.props.onItemClick(categoryId);
  }

  render() {
    const { data } = this.props;
    const { isProgressVisible } = this.state;
    const categoriesList = [];
    if (data.list) {
      data.list.forEach((item) => {
        categoriesList.push(
          <li key={item.id} onClick={this.onCategoryClick(item.id)}>{item.name}</li>
        );
      });
    }
    if (!categoriesList.length) {
      categoriesList.push(<li key="no-categories" className="emptyList">There are no categories yet</li>);
    }
    const categoriesContent = (
      <ul className="categoriesList">
        {categoriesList}
      </ul>
    );
    return (
      <div className="categories">
        <h1>Список категорий</h1>
        {isProgressVisible && <ProgressLoader />}
        {!isProgressVisible && categoriesContent}
      </div>
    );
  }
}

export default CategoriesView;
