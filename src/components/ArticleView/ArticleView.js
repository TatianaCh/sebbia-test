import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import dateFormat from '../../constants/dateFormat';
import ProgressLoader from '../ProgressLoader';
import './ArticleView.scss';


class ArticleView extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isProgressVisible: true,
    };
  }

  componentWillReceiveProps(nextProps) {
    if ('code' in nextProps.data) {
      this.setState({
        isProgressVisible: false,
      });
    }
  }

  render() {
    const { data } = this.props;
    const { isProgressVisible } = this.state;
    if (data.code === 14) {
      return (
        <div className="article">
          <h1>Не удалось найти данную статью</h1>
        </div>
      );
    }
    if (!data.news) {
      return false;
    }
    return (
      <div className="article">
        <h1>{data.news.title}</h1>
        <div className="date">{moment(data.news.date).format(dateFormat)}</div>
        <div className="preview">{data.news.shortDescription}</div>
        <div className="description">
          {isProgressVisible && <ProgressLoader />}
          {!isProgressVisible && <div dangerouslySetInnerHTML={{__html: data.news.fullDescription}} />}
        </div>
      </div>
    );
  }
}

export default ArticleView;
