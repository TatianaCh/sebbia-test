import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import dateFormat from '../../constants/dateFormat';
import ProgressLoader from '../ProgressLoader';
import Pagination from '../Pagination';
import './NewsView.scss';


class NewsView extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    selectedCategory: PropTypes.any.isRequired,
    onItemClick: PropTypes.func.isRequired,
    loadNews: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      isProgressVisible: true,
      page: 0,
      isNextEnable: false,
    };
  }

  componentDidMount() {
    const { selectedCategory, loadNews } = this.props;
    loadNews(selectedCategory, this.state.page);
  }

  componentWillReceiveProps(nextProps, nextState) {
    if ('code' in nextProps.data || this.state.page !== nextState.page) {
      const isNextEnable = nextProps.data.list.length >= 10;
      this.setState({
        isProgressVisible: false,
        isNextEnable,
      });
    }
  }

  onChangePage = difference => () => {
    const { selectedCategory, loadNews } = this.props;
    const page = this.state.page + difference;
    this.setState({
      page,
    });
    loadNews(selectedCategory, page);
  }

  render() {
    const { data, onItemClick } = this.props;
    const { isProgressVisible, page, isNextEnable } = this.state;

    const newsList = [];
    if (data.list) {
      data.list.forEach((item) => {
        newsList.push(
          <li key={item.id} onClick={onItemClick(item)}>
            <h2>{item.title}</h2>
            <div className="date">{moment(item.date).format(dateFormat)}</div>
            <p>{item.shortDescription}</p>
          </li>);
      });
    }
    if (!newsList.length) {
      newsList.push(<li key="no-news" className="emptyList">Пока новостей нет</li>);
    }

    const paginationBlock = !(isNextEnable || page > 0) ? false : (
      <Pagination
        next={this.onChangePage(1)}
        prev={this.onChangePage(-1)}
        page={page}
        nextEnabled={isNextEnable}
      />
    );
    const listContent = (
      <div>
        <ul className="newsList">
          {newsList}
        </ul>
        {paginationBlock}
      </div>
    );
    return (
      <div>
        <h1>Список Новостей</h1>
        {isProgressVisible && <ProgressLoader />}
        {!isProgressVisible && listContent}
      </div>
    );
  }
}

export default NewsView;
